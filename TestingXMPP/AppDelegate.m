//
//  AppDelegate.m
//  TestingXMPP
//
//  Created by Mohd Zharif Anuar on 12/5/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize xmppStream;
@synthesize xmppRoster;
@synthesize chatDelegate;
@synthesize messageDelegate;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [self disconnect];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    if([self connect])
    {
//        [self didconn];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - xmpp section
-(void)setupStream
{
    xmppStream = [[XMPPStream alloc] init];
    [self.xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
//    XMPPFileTransferStreamMethodBytestreams
//    [XMPPTransports s];
}

-(void)goOnline
{
    XMPPPresence *presence = [XMPPPresence presence];
    [self.xmppStream sendElement:presence];
}

-(void)goOffline
{
    XMPPPresence *presence = [XMPPPresence presenceWithType:@"unavailable"];
    [self.xmppStream sendElement:presence];
}

-(BOOL)connect
{
    xmppStream = [[XMPPStream alloc] init];
    [xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
//    [self.xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    NSString *jabberID = [[NSUserDefaults standardUserDefaults] stringForKey:@"userID"];
    NSString *myPassword = [[NSUserDefaults standardUserDefaults] stringForKey:@"userPassword"];
    
    if (jabberID == nil || myPassword == nil)
    {
        return false;
    }
    
    if (![xmppStream isDisconnected])
    {
        return true;
    }
    
//    [self.xmppStream setMyJID:[XMPPJID jidWithString:jabberID]];
    [xmppStream setMyJID:[XMPPJID jidWithString:jabberID]];
    password = myPassword;
    
    [xmppStream setHostName:@"175.138.29.54"];
    [xmppStream setHostPort:5222];
//    [self.xmppStream setHostName:@"175.138.29.54"];
//    [self.xmppStream setHostPort:5222];
    
    XMPPMessageDeliveryReceipts* xmppMessageDeliveryRecipts = [[XMPPMessageDeliveryReceipts alloc] initWithDispatchQueue:dispatch_get_main_queue()];
    xmppMessageDeliveryRecipts.autoSendMessageDeliveryReceipts = true;
    xmppMessageDeliveryRecipts.autoSendMessageDeliveryRequests = true;
    [xmppMessageDeliveryRecipts activate:xmppStream];
    
    NSError *error = nil;
//    if (![self.xmppStream connectWithTimeout:30 error:&error])
    if (![xmppStream connectWithTimeout:30 error:&error])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:[NSString stringWithFormat:@"Can't connect to server %@", [error localizedDescription]]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        return false;
    }
    NSLog(@"connected");
    return true;
}

-(void)updateHostPort
{
    
}

- (void)disconnect
{
    [self goOffline];
    [xmppStream disconnect];
    
    NSLog(@"disconnected");
    [chatDelegate didDisconnect];
}


#pragma mark - XMPP delegates
-(void)xmppStreamDidConnect:(XMPPStream *)sender
{
    NSLog(@"xmppStreamDidConnect %@", sender);
    
    isOpen = true;
    NSError *error = nil;
//    [self.xmppStream authenticateWithPassword:password error:&error];
    [xmppStream authenticateWithPassword:password error:&error];
}

-(void)xmppStreamDidAuthenticate:(XMPPStream *)sender
{
    NSLog(@"xmppStreamDidAuthenticate %@", sender);
    
    [self goOnline];
}

-(BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq
{
    NSLog(@"didReceiveIQ %@, \n\n %@", sender, iq);
    return NO;
}

-(void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
    //receive message when someone send message to the server
    NSLog(@"appdelegate didReceiveMessage\n\n%@\n\n", message);
    
    NSString *msg = [[message elementForName:@"body"] stringValue];
    NSString *from = [[message attributeForName:@"from"] stringValue];
    
    NSMutableDictionary *m = [[NSMutableDictionary alloc] init];
//    [m setObject:msg forKey:@"msg"];
//    [m setObject:from forKey:@"sender"];
//    
    [messageDelegate newMessageReceived:m];
    
//    NSString *strgCurrentPage = @"";
//    if ([strgCurrentPage isEqualToString:@"Page Chatting"])
//    {
//        //need to display previous message + new message & store into DB
//        //call delegate
//    }
//    else if ([strgCurrentPage isEqualToString:@"Page Chatting history"])
//    {
//        //need to display has new message & store into DB
//        //call delegate
//    }
//    else
//    {
//        //store into DB
//    }
    
    //-(void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message {
    //    // message recived
    //    if([[message elementForName:@"body"]stringValue] != nil){
    //        NSString *msg = [[message elementForName:@"body"] stringValue];
    //        NSString *from = [[message attributeForName:@"from"] stringValue];
    //        NSLog(@"%@ %@",msg,from);
    //
    //        NSMutableDictionary *m = [[NSMutableDictionary alloc] init];
    //        [m setObject:msg forKey:@"msg"];
    //        [m setObject:from forKey:@"sender"];
    //
    //        [_messageDelegate newMessageRecived:m];
    //    }
    //}
    
    //-(void)newMessageRecived:(NSDictionary *)messageContent{
    //    [messages addObject:messageContent];
    //    [self.tView reloadData];
    //}
}

-(void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
    //receive info about whos online offline from server
    
//    NSLog(@"\n\nappdelegate didReceivePresence\n\n%@\n\n%@", [presence elementID], presence);
    
    NSString *presenceType = [presence type]; // online/offline
    NSString *myUsername = [[sender myJID] user];
    NSString *presenceFromUser = [[presence from] user];
    
    if (![presenceFromUser isEqualToString:myUsername]) {
        
        if ([presenceType isEqualToString:@"available"]) {
            
            [chatDelegate newBuddyOnline:[NSString stringWithFormat:@"%@@%@", presenceFromUser, @"server"]];
            
        } else if ([presenceType isEqualToString:@"unavailable"]) {
            
            [chatDelegate buddyWentOffline:[NSString stringWithFormat:@"%@@%@", presenceFromUser, @"server"]];
        }
    }
    
//    [xmppRoster addDelegate:self delegateQueue:dispatch_get_main_queue()];
//    [xmppRoster fetchRoster];
    
    if([xmppRoster hasRoster])
    {
        NSLog(@"hasRoster");
    }
    
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didAddResource:(XMPPResourceMemoryStorageObject *)resource  withUser:(XMPPUserMemoryStorageObject *)user
{
    NSLog(@"appdelegate didAddResource\n\n%@\n\n%@", resource, user);
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didUpdateResource:(XMPPResourceMemoryStorageObject *)resource withUser:(XMPPUserMemoryStorageObject *)user
{
    NSLog(@"appdelegate didUpdateResource\n\n%@\n\n%@", resource, user);
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didRemoveResource:(XMPPResourceMemoryStorageObject *)resource withUser:(XMPPUserMemoryStorageObject *)user
{
    NSLog(@"appdelegate didRemoveResource\n\n%@\n\n%@", resource, user);
}

@end

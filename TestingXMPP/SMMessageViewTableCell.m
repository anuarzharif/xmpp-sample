//
//  SMMessageViewTableCell.m
//  TestingXMPP
//
//  Created by Mohd Zharif Anuar on 12/8/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import "SMMessageViewTableCell.h"

@implementation SMMessageViewTableCell

@synthesize lblTime, lblMessage, messageContentView, bgImageView;

//- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
//    
//    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
//        
//        senderAndTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 300, 20)];
//        senderAndTimeLabel.textAlignment = NSTextAlignmentCenter;
//        senderAndTimeLabel.font = [UIFont systemFontOfSize:11.0];
//        senderAndTimeLabel.textColor = [UIColor lightGrayColor];
//        [self.contentView addSubview:senderAndTimeLabel];
//        
//        bgImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
//        [self.contentView addSubview:bgImageView];
//        
//        messageContentView = [[UITextView alloc] init];
//        messageContentView.backgroundColor = [UIColor clearColor];
//        messageContentView.editable = NO;
//        messageContentView.scrollEnabled = NO;
//        [messageContentView sizeToFit];
//        [self.contentView addSubview:messageContentView];
//        
//    }
//    
//    return self;
//    
//}

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    
//    senderAndTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 300, 20)];
//    senderAndTimeLabel.textAlignment = NSTextAlignmentCenter;
//    senderAndTimeLabel.font = [UIFont systemFontOfSize:11.0];
//    senderAndTimeLabel.textColor = [UIColor lightGrayColor];
//    [self.contentView addSubview:senderAndTimeLabel];
//    
//    bgImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
//    [self.contentView addSubview:bgImageView];
//    
//    messageContentView = [[UITextView alloc] init];
//    messageContentView.backgroundColor = [UIColor clearColor];
//    messageContentView.editable = NO;
//    messageContentView.scrollEnabled = NO;
//    [messageContentView sizeToFit];
//    [self.contentView addSubview:messageContentView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

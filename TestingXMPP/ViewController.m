//
//  ViewController.m
//  TestingXMPP
//
//  Created by Mohd Zharif Anuar on 12/5/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    XMPPStream *xmppStream;
    XMPPJID *jid;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
//    JabberClientAppDelegate *del = [self appDelegate];
//    del._messageDelegate = self;
//    [self.messageField becomeFirstResponder];
    
//    xmppStream = [[XMPPStream alloc] init];
//    [xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
//    
////    jid = [XMPPJID jidWithString:@"cesare@YOURSERVER"];
////    jid = [XMPPJID jidWithString:@"admin"];
//    xmppStream.myJID = [XMPPJID jidWithString:@"user@dev1.myCompany.com"];
//    xmppStream.hostName = @"175.138.29.54";
////    xmppStream.hostName = @"http://192.168.0.120";
//    xmppStream.hostPort = 5222;
//    
//    NSLog(@"Attempting TURN connection to %@", jid);
//    
//    TURNSocket *turnSocket = [[TURNSocket alloc] initWithStream:xmppStream toJID:jid];
//    
////    [turnSockets addObject:turnSocket];
//    
//    [turnSocket startWithDelegate:self delegateQueue:dispatch_get_main_queue()];
//    
//    NSError *error = nil;
//    if (![xmppStream connectWithTimeout:20 error:&error])
//    {
//        NSLog(@"Oops, I probably forgot something: %@", error);
//    }
}

- (void)turnSocket:(TURNSocket *)sender didSucceed:(GCDAsyncSocket *)socket {
    
    NSLog(@"TURN Connection succeeded!");
    NSLog(@"You now have a socket that you can use to send/receive data to/from the other person.");
    
//    [turnSockets removeObject:sender];
}

- (void)turnSocketDidFail:(TURNSocket *)sender {
    
    NSLog(@"TURN Connection failed!\n\n%@\n\n", sender);
//    [turnSockets removeObject:sender];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

//
//  SMLoginViewController.h
//  TestingXMPP
//
//  Created by Mohd Zharif Anuar on 12/8/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface SMLoginViewController : UIViewController
{
    UITextField *loginField;
    UITextField *passwordField;
}

@property (nonatomic,retain) IBOutlet UITextField *loginField;
@property (nonatomic,retain) IBOutlet UITextField *passwordField;

-(IBAction)login;
-(IBAction)hideLogin;

@end

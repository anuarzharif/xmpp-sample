//
//  SMMessageViewTableCell.h
//  TestingXMPP
//
//  Created by Mohd Zharif Anuar on 12/8/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMMessageViewTableCell : UITableViewCell
{
    
}

@property (nonatomic,assign) IBOutlet UILabel *lblTime;
@property (nonatomic,assign) IBOutlet UILabel *lblMessage;
@property (nonatomic,assign) UITextView *messageContentView;
@property (nonatomic,assign) UIImageView *bgImageView;

@end

//
//  SMMessageDelegate.h
//  TestingXMPP
//
//  Created by Mohd Zharif Anuar on 12/8/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMMessageDelegate <NSObject>

- (void)newMessageReceived:(NSDictionary *)messageContent;

@end

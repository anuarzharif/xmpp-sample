//
//  SMBuddyListViewController.m
//  TestingXMPP
//
//  Created by Mohd Zharif Anuar on 12/8/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import "SMBuddyListViewController.h"

#import "ChatPage.h"

@interface SMBuddyListViewController ()
{
    AppDelegate *appDelegate;
    
    NSMutableArray *arrOnlineBuddies;
}

@end

@implementation SMBuddyListViewController

@synthesize tView, addBuddyView, buddyField;

//-(AppDelegate *)appDelegate
//{
//    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
//}

//- (XMPPStream *)xmppStream
//{
//    return [[self appDelegate] xmppStream];
//}
//
//- (XMPPRoster *)xmppRoster {
//    return [[self appDelegate] xmppRoster];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setVariable];
    [self setGraphic];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSString *login = [[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    if (login)
    {
        if ([appDelegate connect])
        {
            NSLog(@"show buddy list");
        }
    }
    else
    {
        
        [self showLogin];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)setVariable
{
//    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];// [self appDelegate];
    appDelegate.chatDelegate = self;
    
    arrOnlineBuddies = [[NSMutableArray alloc ] init];
}

-(void)setGraphic
{
    self.title = @"Buddy List";
    UIBarButtonItem *btnRight = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                              target:self
                                                                              action:@selector(showLogin)];
    self.navigationItem.rightBarButtonItem = btnRight;
}

#pragma mark - tableview section
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrOnlineBuddies count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *s = (NSString *) [arrOnlineBuddies objectAtIndex:indexPath.row];
    
    static NSString *CellIdentifier = @"UserCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = s;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
//    SMChatViewController *chatController = [[SMChatViewController alloc] initWithUser:userName];
    
//    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    SMChatViewController *chatController = [storyBoard instantiateViewControllerWithIdentifier:@"SMChatViewController"];
//    [self showViewController:chatController sender:self];
//    [self presentViewController:chatController animated:true completion:nil];
    
    NSString *userName = (NSString *)[arrOnlineBuddies objectAtIndex:indexPath.row];
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChatPage *noname = [storyBoard instantiateViewControllerWithIdentifier:@"ChatPage"];
    noname.strgUser = @"dv02@localhost.localdomain";
    [self showViewController:noname sender:self];
}

#pragma mark - button section
-(IBAction)showLogin
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SMLoginViewController *loginController = [storyBoard instantiateViewControllerWithIdentifier:@"SMLoginViewController"];
    [self presentViewController:loginController animated:true completion:nil];
}

//-(IBAction)addBuddy
//{
//    //    XMPPJID *newBuddy = [XMPPJID jidWithString:self.buddyField.text];
//    //    [self.xmppRoster addBuddy:newBuddy withNickname:@"ciao"];
//    
//}

#pragma mark - chatlist delegate
-(void)newBuddyOnline:(NSString *)buddyName
{
    if (![arrOnlineBuddies containsObject:buddyName])
    {
        [arrOnlineBuddies addObject:buddyName];
        [self.tView reloadData];
    }
}

-(void)buddyWentOffline:(NSString *)buddyName
{
    [arrOnlineBuddies removeObject:buddyName];
    [self.tView reloadData];
}

-(void)didDisconnect
{
    [arrOnlineBuddies removeAllObjects];
    [self.tView reloadData];
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  SMChatViewController.m
//  TestingXMPP
//
//  Created by Mohd Zharif Anuar on 12/8/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import "SMChatViewController.h"

#import "NSString+Utils.h"

@interface SMChatViewController ()

@end

@implementation SMChatViewController

@synthesize messageField, chatWithUser, tView;


- (AppDelegate *)appDelegate {
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (XMPPStream *)xmppStream
{
    return [[self appDelegate] xmppStream];
}

- (id) initWithUser:(NSString *) userName {
    
    if (self = [super init]) {
        
//        chatWithUser = userName; // @ missing
        chatWithUser = @"dv02@175.138.29.54";
        turnSockets = [[NSMutableArray alloc] init];
    }
    
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Chat Page";
    
//    UIBarButtonItem *btnRight = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemReply
//                                                                              target:self
//                                                                              action:@selector(sendMessage)];
//    self.navigationItem.rightBarButtonItem = btnRight;
    
    
    self.tView.delegate = self;
    self.tView.dataSource = self;
    [self.tView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    messages = [[NSMutableArray alloc ] init];
    
    AppDelegate *del = [self appDelegate];
    del.messageDelegate = self;
    
    self.messageField.delegate = self;
//    [self.messageField becomeFirstResponder];
    
//    XMPPJID *jid = [XMPPJID jidWithString:@"cesare@175.138.29.54"];
    XMPPJID *jid = [XMPPJID jidWithString:@"dv01@175.138.29.54"];
//    [xmppStream setHostName:@"localhost"];
//    [xmppStream setHostPort:5222];
//    
//    XMPPStream *xmppStream = xmppStream = [[XMPPStream alloc] init];
//    [xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()]; .myJID = [XMPPJID jidWithString:@"user@dev1.myCompany.com"];
//    xmppStream.hostName = @"175.138.29.54";
//    xmppStream.hostName = @"http://192.168.0.120";
//    xmppStream.hostPort = 5222;
    
//    NSLog(@"Attempting TURN connection to %@", jid);
    
    TURNSocket *turnSocket = [[TURNSocket alloc] initWithStream:[self xmppStream] toJID:jid];
    [turnSockets addObject:turnSocket];
    
    [turnSocket startWithDelegate:self delegateQueue:dispatch_get_main_queue()];
}

//+ (BOOL)isNewStartTURNRequest:(XMPPIQ *)iq;
//
//+ (NSArray *)proxyCandidates;
//+ (void)setProxyCandidates:(NSArray *)candidates;
//
//- (id)initWithStream:(XMPPStream *)xmppStream toJID:(XMPPJID *)jid;
//- (id)initWithStream:(XMPPStream *)xmppStream incomingTURNRequest:(XMPPIQ *)iq;
//
//- (void)startWithDelegate:(id)aDelegate delegateQueue:(dispatch_queue_t)aDelegateQueue;
//
//- (BOOL)isClient;
//
//- (void)abort;

- (void)turnSocket:(TURNSocket *)sender didSucceed:(GCDAsyncSocket *)socket {
    
    NSLog(@"TURN Connection succeeded!");
    NSLog(@"You now have a socket that you can use to send/receive data to/from the other person.");
    
    [turnSockets removeObject:sender];
}

- (void)turnSocketDidFail:(TURNSocket *)sender {
    
//    NSLog(@"TURN Connection failed! %@", [sender description]);
    [turnSockets removeObject:sender];
    
}



#pragma mark -
#pragma mark Actions

- (IBAction) closeChat {
    
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)sendMessage {
    
    NSString *messageStr = self.messageField.text;
    
    if([messageStr length] > 0) {
        
        NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
        [body setStringValue:messageStr];
        
        NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
        [message addAttributeWithName:@"type" stringValue:@"chat"];
//        [message addAttributeWithName:@"to" stringValue:chatWithUser];
        [message addAttributeWithName:@"to" stringValue:@"dv02@localhost.localdomain"];
        [message addChild:body];
        
        NSLog(@"testing sahaja %@", chatWithUser);
        
        [self.xmppStream sendElement:message];
        
        self.messageField.text = @"";
        
        
        NSMutableDictionary *m = [[NSMutableDictionary alloc] init];
        [m setObject:[messageStr substituteEmoticons] forKey:@"msg"];
        [m setObject:@"you" forKey:@"sender"];
        [m setObject:[NSString getCurrentTime] forKey:@"time"];
        
        [messages addObject:m];
        [self.tView reloadData];
        
    }
    
    NSIndexPath *topIndexPath = [NSIndexPath indexPathForRow:messages.count-1
                                                   inSection:0];
    
    [self.tView scrollToRowAtIndexPath:topIndexPath
                      atScrollPosition:UITableViewScrollPositionMiddle
                              animated:YES];
}


#pragma mark -
#pragma mark Table view delegates

static CGFloat padding = 20.0;


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSDictionary *s = (NSDictionary *) [messages objectAtIndex:indexPath.row];
    
    static NSString *CellIdentifier = @"MessageCellIdentifier";
    
    SMMessageViewTableCell *cell = (SMMessageViewTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[SMMessageViewTableCell alloc] init];
    }
    
    NSString *sender = [s objectForKey:@"sender"];
    NSString *message = [s objectForKey:@"msg"];
    NSString *time = [s objectForKey:@"time"];
    
    CGSize  textSize = { 260.0, 10000.0 };
    CGSize size = [message sizeWithFont:[UIFont boldSystemFontOfSize:13]
                      constrainedToSize:textSize
                          lineBreakMode:UILineBreakModeWordWrap];
    
    
    size.width += (padding/2);
    
    
    cell.messageContentView.text = message;
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.userInteractionEnabled = NO;
    
    
    UIImage *bgImage = nil;
    
    
    if ([sender isEqualToString:@"you"]) { // left aligned
        
        bgImage = [[UIImage imageNamed:@"orange.png"] stretchableImageWithLeftCapWidth:24  topCapHeight:15];
        
        [cell.messageContentView setFrame:CGRectMake(padding, padding*2, size.width, size.height)];
        
        [cell.bgImageView setFrame:CGRectMake( cell.messageContentView.frame.origin.x - padding/2,
                                              cell.messageContentView.frame.origin.y - padding/2,
                                              size.width+padding,
                                              size.height+padding)];
        
    } else {
        
        bgImage = [[UIImage imageNamed:@"aqua.png"] stretchableImageWithLeftCapWidth:24  topCapHeight:15];
        
        [cell.messageContentView setFrame:CGRectMake(320 - size.width - padding,
                                                     padding*2,
                                                     size.width,
                                                     size.height)];
        
        [cell.bgImageView setFrame:CGRectMake(cell.messageContentView.frame.origin.x - padding/2,
                                              cell.messageContentView.frame.origin.y - padding/2,
                                              size.width+padding,
                                              size.height+padding)];
        
    }
    
//    cell.bgImageView.image = bgImage;
    
    cell.lblMessage.text = [NSString stringWithFormat:@"%@ %@", sender, time];
    cell.lblTime.text = [NSString stringWithFormat:@"%@ %@", sender, time];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dict = (NSDictionary *)[messages objectAtIndex:indexPath.row];
    NSString *msg = [dict objectForKey:@"msg"];
    
    CGSize  textSize = { 260.0, 10000.0 };
    CGSize size = [msg sizeWithFont:[UIFont boldSystemFontOfSize:13]
                  constrainedToSize:textSize
                      lineBreakMode:UILineBreakModeWordWrap];
    
    size.height += padding*2;
    
    CGFloat height = size.height < 65 ? 65 : size.height;
    return height;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [messages count];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}


#pragma mark -
#pragma mark Chat delegates


- (void)newMessageReceived:(NSDictionary *)messageContent {
    
    NSString *m = [messageContent objectForKey:@"msg"];
    
    NSMutableDictionary *dictMsgContent = [[NSMutableDictionary alloc] initWithDictionary:messageContent];
    [dictMsgContent setObject:[m substituteEmoticons] forKey:@"msg"];
    [dictMsgContent setObject:[NSString getCurrentTime] forKey:@"time"];
    [messages addObject:messageContent];
    [self.tView reloadData];
    
    NSIndexPath *topIndexPath = [NSIndexPath indexPathForRow:messages.count-1 
                                                   inSection:0];
    
    [self.tView scrollToRowAtIndexPath:topIndexPath 
                      atScrollPosition:UITableViewScrollPositionMiddle 
                              animated:YES];
}

#pragma mark - textfield section
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end

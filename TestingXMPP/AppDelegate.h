//
//  AppDelegate.h
//  TestingXMPP
//
//  Created by Mohd Zharif Anuar on 12/5/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SMChatDelegate.h"
#import "SMMessageDelegate.h"

@class SMBuddyListViewController;

@import XMPPFramework;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    NSString *password;
    
    BOOL isOpen;
    
//    XMPPStream *xmppStream;
//    XMPPRoster *xmppRoster;
}

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, readonly) XMPPStream *xmppStream;
@property (nonatomic, readonly) XMPPRoster *xmppRoster;

@property (nonatomic, assign) NSObject <SMChatDelegate> *chatDelegate;
@property (nonatomic, assign) NSObject <SMMessageDelegate> *messageDelegate;

-(BOOL)connect;
-(void)disconnect;

@end


//
//  SMBuddyListViewController.h
//  TestingXMPP
//
//  Created by Mohd Zharif Anuar on 12/8/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "SMLoginViewController.h"
#import "SMChatViewController.h"
#import "SMChatDelegate.h"

@import XMPPFramework;

@interface SMBuddyListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, SMChatDelegate>
{
    UITableView *tView;
    
    UIView		*addBuddyView;
    UITextField *buddyField;
}

@property (nonatomic,retain) IBOutlet UITableView *tView;

@property (nonatomic,retain) IBOutlet UIView *addBuddyView;
@property (nonatomic,retain) IBOutlet UITextField *buddyField;

-(IBAction)showLogin;

@end

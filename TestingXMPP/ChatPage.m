//
//  ChatPage.m
//  TestingXMPP
//
//  Created by Mohd Zharif Anuar on 12/22/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import "ChatPage.h"
#import "NSString+Utils.h"

#import "CustomCell.h"

@interface ChatPage ()
{
    AppDelegate *appDelegate;
    XMPPStream *xmppStream;
    
    NSMutableArray *arrMessages;
    
    NSString *strgMyName;
}

@end

@implementation ChatPage

@synthesize strgUser;

//-(AppDelegate *)appDelegate
//{
//    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
//}
//
//-(XMPPStream *)xmppStream
//{
//    return [[self appDelegate] xmppStream];
//}

-(void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setVariable];
    [self setGraphic];
}

-(void)setVariable
{
    arrMessages = [[NSMutableArray alloc ] init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.messageDelegate = self;
    xmppStream = appDelegate.xmppStream;
    
    strgMyName = @"";
}

-(void)setGraphic
{
    self.title = @"Chat Page (2)";
    
    NSLog(@"im chatting with %@", strgUser);
}

#pragma mark Table view delegates
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrMessages count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomCell *cell;
    
    if ([strgMyName isEqualToString:@""])
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"CustomCell2"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.lblDisplay.text = [NSString stringWithFormat:@"testing sahaja la oi %ld", (long)indexPath.row];
        cell.lblDisplay2.text = [NSString stringWithFormat:@"testing sahaja la oi %ld", (long)indexPath.section];
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"CustomCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.lblDisplay.text = [NSString stringWithFormat:@"testing sahaja la oi %ld", (long)indexPath.row];
        cell.lblDisplay2.text = [NSString stringWithFormat:@"testing sahaja la oi %ld", (long)indexPath.section];
    }
    
    return cell;
    
//    NSDictionary *s = (NSDictionary *) [messages objectAtIndex:indexPath.row];
//    
//    static NSString *CellIdentifier = @"MessageCellIdentifier";
//    
//    SMMessageViewTableCell *cell = (SMMessageViewTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    
//    if (cell == nil) {
//        cell = [[SMMessageViewTableCell alloc] init];
//    }
//    
//    NSString *sender = [s objectForKey:@"sender"];
//    NSString *message = [s objectForKey:@"msg"];
//    NSString *time = [s objectForKey:@"time"];
//    
//    CGSize  textSize = { 260.0, 10000.0 };
//    CGSize size = [message sizeWithFont:[UIFont boldSystemFontOfSize:13]
//                      constrainedToSize:textSize
//                          lineBreakMode:UILineBreakModeWordWrap];
//    
//    
//    size.width += (padding/2);
//    
//    
//    cell.messageContentView.text = message;
//    cell.accessoryType = UITableViewCellAccessoryNone;
//    cell.userInteractionEnabled = NO;
//    
//    
//    UIImage *bgImage = nil;
//    
//    
//    if ([sender isEqualToString:@"you"]) { // left aligned
//        
//        bgImage = [[UIImage imageNamed:@"orange.png"] stretchableImageWithLeftCapWidth:24  topCapHeight:15];
//        
//        [cell.messageContentView setFrame:CGRectMake(padding, padding*2, size.width, size.height)];
//        
//        [cell.bgImageView setFrame:CGRectMake( cell.messageContentView.frame.origin.x - padding/2,
//                                              cell.messageContentView.frame.origin.y - padding/2,
//                                              size.width+padding,
//                                              size.height+padding)];
//        
//    } else {
//        
//        bgImage = [[UIImage imageNamed:@"aqua.png"] stretchableImageWithLeftCapWidth:24  topCapHeight:15];
//        
//        [cell.messageContentView setFrame:CGRectMake(320 - size.width - padding,
//                                                     padding*2,
//                                                     size.width,
//                                                     size.height)];
//        
//        [cell.bgImageView setFrame:CGRectMake(cell.messageContentView.frame.origin.x - padding/2,
//                                              cell.messageContentView.frame.origin.y - padding/2,
//                                              size.width+padding,
//                                              size.height+padding)];
//        
//    }
//    
//    //    cell.bgImageView.image = bgImage;
//    
//    cell.lblMessage.text = [NSString stringWithFormat:@"%@ %@", sender, time];
//    cell.lblTime.text = [NSString stringWithFormat:@"%@ %@", sender, time];
//    
//    return cell;
    
}

#pragma mark Chat delegates
-(void)newMessageReceived:(NSDictionary *)messageContent
{
    NSString *m = [messageContent objectForKey:@"msg"];
    
    NSMutableDictionary *dictMsgContent = [[NSMutableDictionary alloc] initWithDictionary:messageContent];
//    [dictMsgContent setObject:[m substituteEmoticons] forKey:@"msg"];
//    [dictMsgContent setObject:[NSString getCurrentTime] forKey:@"time"];
    [arrMessages addObject:messageContent];
    [tvDisplay reloadData];
    
    NSIndexPath *topIndexPath = [NSIndexPath indexPathForRow:[arrMessages count] - 1
                                                   inSection:0];
    
    [tvDisplay scrollToRowAtIndexPath:topIndexPath
                     atScrollPosition:UITableViewScrollPositionMiddle
                             animated:YES];
}

#pragma mark - button section
-(IBAction)sendMessage
{
    NSString *strgMessage = txtDisplay.text;
    
    if ([strgMessage length] > 0)
    {
        NSString *messageID = [xmppStream generateUUID];
        
        NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
        [body setStringValue:strgMessage];
        
        NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
        [message addAttributeWithName:@"id" stringValue:messageID];
        [message addAttributeWithName:@"type" stringValue:@"chat"];
        [message addAttributeWithName:@"to" stringValue:strgUser];
//        [message addAttributeWithName:@"to" stringValue:@"dv02@localhost.localdomain"];
        [message addChild:body];
        
        NSLog(@"testing sahaja %@", strgUser);
        
//        [self.xmppStream sendElement:message];
        [xmppStream sendElement:message];
        
        txtDisplay.text = @"";
        
        NSMutableDictionary *m = [[NSMutableDictionary alloc] init];
        [m setObject:[strgMessage substituteEmoticons] forKey:@"msg"];
//        [m setObject:@"you" forKey:@"sender"];
//        [m setObject:[NSString getCurrentTime] forKey:@"time"];
        
        [arrMessages addObject:m];
        [tvDisplay reloadData];
    }
    
    NSIndexPath *topIndexPath = [NSIndexPath indexPathForRow:[arrMessages count] - 1
                                                   inSection:0];
    
    [tvDisplay scrollToRowAtIndexPath:topIndexPath
                     atScrollPosition:UITableViewScrollPositionMiddle
                             animated:true];
    
    
//    <message xmlns="jabber:client" id="0hBjw-6095" to="dv01@localhost.localdomain" type="chat" from="st65@localhost.localdomain/Smack"><body>#!#|uPd4tE@p12op1l|353517060180946|+60193471433</body><delay xmlns="urn:xmpp:delay" from="localhost.localdomain" stamp="2016-12-22T07:17:38.627Z"></delay></message>
    
//    <message xmlns="jabber:client" type="chat" to="dv01@localhost.localdomain" from="dv02@localhost.localdomain/9x4y41dqmv"><body>test</body></message>
    
//    <message xmlns="jabber:client" type="chat" to="dv01@localhost.localdomain" from="dv02@localhost.localdomain/1vvp6n9p6g"><body>power</body><delay xmlns="urn:xmpp:delay" from="localhost.localdomain" stamp="2016-12-23T01:47:47.940Z"></delay></message>
}

#pragma mark - textfield section
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

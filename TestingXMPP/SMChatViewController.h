//
//  SMChatViewController.h
//  TestingXMPP
//
//  Created by Mohd Zharif Anuar on 12/8/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "SMBuddyListViewController.h"
#import "SMMessageViewTableCell.h"

@import XMPPFramework;

@interface SMChatViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, SMMessageDelegate, UITextFieldDelegate> {
    
    UITextField		*messageField;
    NSString		*chatWithUser;
    UITableView		*tView;
    NSMutableArray	*messages;
    NSMutableArray *turnSockets;
    
    IBOutlet UIView *vwTextField;
}

@property (nonatomic,retain) IBOutlet UITextField *messageField;
@property (nonatomic,retain) NSString *chatWithUser;
@property (nonatomic,retain) IBOutlet UITableView *tView;

- (id) initWithUser:(NSString *) userName;
- (IBAction) sendMessage;
- (IBAction) closeChat;

@end

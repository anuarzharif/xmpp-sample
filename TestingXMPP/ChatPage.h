//
//  ChatPage.h
//  TestingXMPP
//
//  Created by Mohd Zharif Anuar on 12/22/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

@import XMPPFramework;

@interface ChatPage : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, SMMessageDelegate>
{
    IBOutlet UITableView *tvDisplay;
    
    IBOutlet UITextField *txtDisplay;
}

@property (nonatomic) NSString *strgUser;

-(IBAction)sendMessage;

@end

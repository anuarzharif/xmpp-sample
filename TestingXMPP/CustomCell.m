//
//  CustomCell.m
//  TestingXMPP
//
//  Created by Mohd Zharif Anuar on 12/22/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell

@synthesize lblDisplay, lblDisplay2;

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
